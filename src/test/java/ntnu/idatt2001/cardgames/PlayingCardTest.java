package ntnu.idatt2001.cardgames;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Playing card test.
 * @author amtvedt
 */
public class PlayingCardTest {

    /**
     * Test the method get as string.
     */
    @Test
    public void testGetAsString()  {
        PlayingCard card = new PlayingCard('S', 11);
        assertEquals(card.getAsString(), "S11");
    }

    /**
     * Test illegal exception handling.
     */
    @Test
    public void testIllegalExceptionHandling(){
        assertThrows(IllegalArgumentException.class, () -> {
            new PlayingCard('S', 14);
            new PlayingCard('b', 10);
            new PlayingCard('S', 0);});
    }

}