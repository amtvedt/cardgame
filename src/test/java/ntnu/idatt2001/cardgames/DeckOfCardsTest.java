package ntnu.idatt2001.cardgames;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Deck of cards test.
 * @author amtvedt
 */
public class DeckOfCardsTest {

    /**
     * Test the method deal hand.
     */
    @Test
    public void testDealHand() {
        DeckOfCards test = new DeckOfCards();
        assertEquals(10, test.dealHand(10).size());
        HashSet<PlayingCard> test2 = new HashSet<>(test.dealHand(10)); //asserts that there is no duplicates in the hand.
        assertEquals(10, test2.size());
    }

    /**
     * Test get card stack.
     */
    @Test
    public void testGetCardStack(){
        DeckOfCards test = new DeckOfCards();
        assertEquals(52,test.getCardsStack().size());
    }

    /**
     * Test get a random card.
     */
    @Test
    public void testGetRandomCard(){
        DeckOfCards test = new DeckOfCards();
        assertNotNull(test.getRandomCardFromDeck());
    }

    /**
     * Test get size of deck left.
     */
    @Test
    public void testGetSizeOfDeckLeft(){
        DeckOfCards test = new DeckOfCards();
        assertEquals(test.getSizeOfDeckLeft(), 52);
        test.dealHand(12);
        assertEquals(test.getSizeOfDeckLeft(), 40);
    }

    /**
     * Test illegal argument exception.
     */
    @Test
    public void testIllegalArgumentException(){
        DeckOfCards test = new DeckOfCards();
        test.dealHand(50);
        assertThrows(IllegalArgumentException.class, () -> {
            new DeckOfCards().dealHand(4);
            new DeckOfCards().dealHand(53);
            test.dealHand(10);
        });
    }
}