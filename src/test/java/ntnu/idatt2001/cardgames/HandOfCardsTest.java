package ntnu.idatt2001.cardgames;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Hand of cards test.
 * @author amtvedt
 */
public class HandOfCardsTest {

    /**
     * Method to create a hand with flush, hearts and spade dame.
     *
     * @return the hand
     */
    private HandOfCards handWithAll(){
        HandOfCards hand = new HandOfCards(5);
        ArrayList<PlayingCard> handToAdd = new ArrayList<>();
        for (int i = 1; i<=5; i++){
            handToAdd.add(new PlayingCard('H',i));
        }
        handToAdd.add(new PlayingCard('S',12));
        hand.setHand(handToAdd);
        return hand;
    }

    /**
     * Method to create a hand without flush, hearts and spade dame.
     *
     * @return the hand
     */
    private HandOfCards handWithNone(){
        HandOfCards hand = new HandOfCards(5);
        ArrayList<PlayingCard> handToAdd = new ArrayList<>();
        for (int i = 1; i<=4; i++){
            handToAdd.add(new PlayingCard('S',i));
        }
        handToAdd.add(new PlayingCard('D',12));
        hand.setHand(handToAdd);
        return hand;
    }

    /**
     * Check flush.
     */
    @Test
    public void checkFlush() {
        HandOfCards hand = handWithAll();
        assertTrue(hand.checkFlush());
        hand = handWithNone();
        assertFalse(hand.checkFlush());
    }

    /**
     * Gets sum.
     */
    @Test
    public void getSum() {
        HandOfCards hand = handWithAll();
        assertEquals(27, hand.getSum());
        hand = handWithNone();
        assertEquals(22, hand.getSum());
    }

    /**
     * Gets hearts.
     */
    @Test
    public void getHearts() {
        HandOfCards hand = handWithAll();
        assertEquals( "H1 H2 H3 H4 H5", hand.getHearts().trim());
        hand = handWithNone();
        assertEquals("No hearts", hand.getHearts());
    }

    /**
     * Check spade dame.
     */
    @Test
    public void checkSpadeDame() {
        HandOfCards hand = handWithAll();
        assertTrue(hand.checkSpadeDame());
        hand = handWithNone();
        assertFalse(hand.checkSpadeDame());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString(){
        HandOfCards hand = handWithAll();
        assertEquals("Hand of cards: H1 H2 H3 H4 H5 S12", hand.toString().trim());
    }

    /**
     * Test set hand.
     */
    @Test
    public void testSetHand(){
        HandOfCards hand = new HandOfCards(5);
        ArrayList<PlayingCard> handToAdd = new ArrayList<>();
        PlayingCard card = new PlayingCard('D', 10);
        handToAdd.add(card);
        for (int i = 1; i<=5; i++){
            handToAdd.add(new PlayingCard('H',i));
        }
        hand.setHand(handToAdd);
        assertEquals(6, hand.getHand().size());
        assertTrue(hand.getHand().contains(card));
    }
}