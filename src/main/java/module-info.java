module ntnu.idatt2001.cardgame.cardgames {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens ntnu.idatt2001.cardgames to javafx.fxml;
    exports ntnu.idatt2001.cardgames;
}