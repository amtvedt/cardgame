package ntnu.idatt2001.cardgames;

import java.util.ArrayList;

/**
 * Represents a hand of cards.
 * @author amtvedt
 */
public class HandOfCards {

    private final ArrayList<PlayingCard> hand;

    /**
     * Instantiates a new Hand of cards with a specified number of cards.
     *
     * @param numberOfCards the number of cards
     */
    public HandOfCards(int numberOfCards) {
        hand = new DeckOfCards().dealHand(numberOfCards);
    }

    /**
     * Checks the hand for flush.
     * If the hand has 20 cards or more, the hand has a flush. If the hand has less than 5 cards, the hand can not have a flush.
     * The method filters the hand for each suit, and checks is there is more than 5 of each sort.
     *
     * @return the boolean
     */
    public boolean checkFlush() {
        boolean result = false;
        if (hand.size() > 20) {
            result = true;
        } else if (hand.size() > 5) {
            for (char s : new DeckOfCards().getSuit()) {
                if (hand.stream().filter(p -> p.getSuit() == s).count() >= 5) {
                    result = true;
                    break;
                }}}
        return result;}

    /**
     * Gets the sum of the hand.
     *
     * @return the sum
     */
    public int getSum() {
        return hand.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * Gets hearts.
     *
     * @return the hearts
     */
    public String getHearts() {
        if (hand.stream().anyMatch(p -> p.getSuit() == 'H')) {
            StringBuilder result = new StringBuilder();
            hand.stream().filter(p -> p.getSuit() == 'H')
                    .map(PlayingCard::getAsString)
                    .forEach(s-> result.append(s).append(" "));
            return result.toString();
        } else {
            return "No hearts";
        }}

    /**
     * Check if the hand holds spade dame.
     *
     * @return the boolean
     */
    public boolean checkSpadeDame() {
        return hand.stream().anyMatch(p -> p.getAsString().equals("S12"));
    }

    /**
     * Gets hand.
     *
     * @return the hand
     */
    public ArrayList<PlayingCard> getHand() {
        return hand;
    }


    public void setHand(ArrayList<PlayingCard> handToSet){
        hand.clear();
        hand.addAll(handToSet);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        hand.stream().map(PlayingCard::getAsString).forEach(p -> result.append(p).append(" "));
        return "Hand of cards: " + result;
    }
}
