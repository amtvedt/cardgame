package ntnu.idatt2001.cardgames;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * The type Card game controller.
 * @author amtvedt
 */
public class CardGameController {

    private HandOfCards hand;

    @FXML
    private Label card1, card2, card3, card4, card5;
    @FXML
    private Label hearts;
    @FXML
    private Label sum;
    @FXML
    private Label queen;
    @FXML
    private Label flush;

    /**
     * Method to deal a hand on button click.
     * Sets the card labels.
     */
    @FXML
    protected void onDealHandClick(){
        resetCheckHand();
        hand = new HandOfCards(5);
        card1.setText(hand.getHand().get(0).getAsString());
        card2.setText(hand.getHand().get(1).getAsString());
        card3.setText(hand.getHand().get(2).getAsString());
        card4.setText(hand.getHand().get(3).getAsString());
        card5.setText(hand.getHand().get(4).getAsString());
    }

    /**
     * Sets the hearts label.
     */
    @FXML
    private void onHeartsClick() {
        hearts.setText(hand.getHearts());
    }

    /**
     * Sets the sum label.
     */
    @FXML
    private void onSumClick(){
        sum.setText(Integer.toString(hand.getSum()));
    }

    /**
     * Sets the queen label.
     */
    @FXML
    private void onQueenClick(){
        if (hand.checkSpadeDame()){
            queen.setText("Yes!");
        }else {
            queen.setText("No..");
        }
    }

    /**
     * Sets the flush label.
     */
    @FXML
    private void onFlushClick(){
        if (hand.checkFlush()){
            flush.setText("Yes!");
        }else {
            flush.setText("No..");
        }
    }

    /**
     * Method to check the hand for flush, hearts, spade dame and sum of the cards.
     */
    @FXML
    protected void onCheckHandClick(){
        onFlushClick();
        onHeartsClick();
        onQueenClick();
        onSumClick();
    }

    /**
     * Reset check hand.
     */
    @FXML
    protected void resetCheckHand(){
        hearts.setText("");
        flush.setText("");
        queen.setText("");
        sum.setText("");
    }
}