package ntnu.idatt2001.cardgames;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of cards.
 * @author amtvedt
 */
public class DeckOfCards {

    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private final ArrayList<PlayingCard> cards;

    /**
     * Instantiates a new Deck of cards.
     */
    public DeckOfCards() {
        cards = new ArrayList<>();
        for (char s: suit){
            for (int i = 1; i<=13; i++){
                cards.add(new PlayingCard(s,i));
            }}
    }

    /**
     * Method to deal a hand with a specified number of cards.
     *
     * @param n the number of cards
     * @return the hand
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if (n<5) {throw new IllegalArgumentException("The number of cards can not be less than 5.");}
        if (n > cards.size()){throw new IllegalArgumentException("There are not " + n + " cards left in the card stack.");}
        ArrayList<PlayingCard> hand = new ArrayList<>();
        while (hand.size() < n) {
            PlayingCard cardToAdd = getRandomCardFromDeck();
            hand.add(cardToAdd);
        }
        return hand;
    }

    /**
     * Method to get a random card from the card stack.
     *
     * @return the Playing card
     */
    public PlayingCard getRandomCardFromDeck(){
        if (cards.isEmpty()){throw new IllegalArgumentException("The card stack is empty.");}
        Random rand = new Random();
        PlayingCard c = this.cards.get(rand.nextInt(cards.size()));
        cards.remove(c);
        return c;
    }

    /**
     * Get size of deck left.
     *
     * @return the size
     */
    public int getSizeOfDeckLeft(){
        return cards.size();
    }

    /**
     * Gets the card stack.
     *
     * @return the cards stack
     */
    public ArrayList<PlayingCard> getCardsStack() {
        return cards;
    }

    /**
     * Get the list of suits.
     *
     * @return the list of suits
     */
    public char[] getSuit() {
        return suit;
    }
}